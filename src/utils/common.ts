import { ColumnDefinition, RowData } from "../Types/Types";

export const isRenderFnValid = (colDef:ColumnDefinition, row:RowData, allowNumbers:boolean = false):boolean => {
    let valueToShow = valueGetterHelper(colDef, row);

     if(
         typeof colDef.render === "function" &&
         typeof colDef.render(valueToShow, row) === "string"
     ){
         return true;
     }
    if (
        typeof colDef.render === "function" &&
        typeof colDef.render(valueToShow, row) !== "string"
    ) {
        if (typeof colDef.render(valueToShow, row) === "object") {
            return true;
        }

        if (allowNumbers && typeof colDef.render(valueToShow, row) === "number") {
            return true;
        }

        console.warn("Incorrect value received from render function.");

        return false;
    }

    return false;
}

export const getValueOfRow = (row:RowData, colDef:ColumnDefinition):string => {
    let rowValue
    let valueToShow = valueGetterHelper(colDef, row)

    if (isRenderFnValid(colDef, row, true)) {
        if (typeof colDef.render(valueToShow, row) === "number") {
            console.warn("The render function returned a number, the grid fallbacks to string.");

            rowValue = colDef.render(valueToShow, row).toString();
        } else {
            rowValue = colDef
                .render(valueToShow, row)
        }
    } else {
        if (typeof valueToShow === 'string') {
            rowValue = valueToShow
        } else if (typeof valueToShow === 'number') {
            console.warn("The grid retrieved a number as a cell value, the grid fallbacks to string conversion.");

            rowValue = valueToShow.toString()
        }
    }

    return rowValue
}

const valueGetterHelper = (colDef:ColumnDefinition, row:RowData): string | number => {
    let valueToShow;

    if (typeof colDef.valueGetter === "function") {
        valueToShow = colDef.valueGetter(row);
    } else if (typeof row[colDef.field] === "string" || typeof row[colDef.field] === "number"){
        valueToShow = row[colDef.field];
    } else if (typeof row[colDef.field] === "object") {
        throw new Error("For object types provide valueGetter().")
    }

    return valueToShow;
}