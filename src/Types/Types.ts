export type RowData = any
export type ShowValue = any;

export interface ColumnDefinition  {
    headerName: string;
    field: string;
    valueGetter?: (row: RowData) => string | number;
    render?: (value: string | number, row:RowData) => string | JSX.Element;
    sort?: boolean;
}

export interface DataGridProps {
    rowData: RowData[];
    columnDefinitions: ColumnDefinition[];
    disabledSearch: boolean;
    searchDelay: number;
}


