import './App.css';
import UserAdministration from "./components/Administration/UserAdministration";
import AdminAdministration from "./components/Administration/AdminAdministration";
import "./style/module.css"
import React from "react";

function App() {
  return (
      <div className="two-table-container">
        <UserAdministration />
        <AdminAdministration />
      </div>

  );
}

export default App;
