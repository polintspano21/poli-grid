// columnDefinitions е структура, която ще казва какви колони ще имаме в таблицата и кое поле от rowData отговаря на конкретна колона
import React, { FunctionComponent } from "react";
import { useCallback, useEffect, useState } from "react";
import { getValueOfRow } from "../../utils/common";
import { SORT_ORDER_ASC, SORT_ORDER_DESC, SORT_ORDER_UNSORT } from "../../constants/commonConstants";
import  "../../style/module.css"
import { ColumnDefinition, DataGridProps, RowData } from "../../Types/Types";


const debounce = (fn:Function, timeout:number = 0, disabled:boolean) =>  {
    if(disabled){
        timeout = 0;
    }
    let timer;
    return function (...args) {
        clearTimeout(timer)
        timer = setTimeout(fn.bind(null, ...args), timeout) // can also use native arguments variable
    }
}

const DataGrid: FunctionComponent<DataGridProps> = ({rowData, columnDefinitions, disabledSearch= true, searchDelay= 0}) => {
    const [sortOrder, setSortOrder] = useState<string>(SORT_ORDER_UNSORT);
    const [displayRowData, setDisplayRowData] = useState<RowData[]>(rowData);
    const [activeColDef, setActiveColDef] = useState<ColumnDefinition>();
    const [filterTextInput, setFilterTextInput] = useState<string>("");
    const [filterCriteria, setFilterCriteria] = useState<string>("");

    const debouncedSetFilterCriteria = useCallback(debounce(setFilterCriteria, searchDelay, false),
        [setFilterCriteria])

    const handleSearch = (value: string) => {
        setFilterTextInput(value);
        debouncedSetFilterCriteria(value);
    };

    useEffect(() => {

        if (activeColDef && activeColDef.sort === false) {
            return;
        }
        if (sortOrder === SORT_ORDER_UNSORT) {
            setDisplayRowData(rowData);
        } else if (sortOrder === SORT_ORDER_ASC) {
            const sortedData = [...rowData].sort((row1, row2) => {
                let row1Value, row2Value;

                row1Value = getValueOfRow(row1, activeColDef);
                row2Value =  getValueOfRow(row2, activeColDef);

                if (row1Value < row2Value) {
                    return -1;
                }
                if (row1Value > row2Value) {
                    return 1;
                }
                return 0;
            });

            setDisplayRowData(sortedData);
        } else if (sortOrder === SORT_ORDER_DESC) {
            const sortedData = [...rowData].sort((row1, row2) => {
                let row1Value, row2Value;
                row1Value = getValueOfRow(row1, activeColDef);
                row2Value =  getValueOfRow(row2, activeColDef);

                if (row1Value > row2Value) {
                    return -1;
                }
                if (row1Value < row2Value) {
                    return 1;
                }
                return 0;
            });
            setDisplayRowData(sortedData);
        }
    }, [activeColDef, rowData, sortOrder, setDisplayRowData]);


    const changeSorting = (colDef:ColumnDefinition) => {
        if (colDef.sort === false) {
            return;
        }
        if (colDef === activeColDef) {
            if (sortOrder === SORT_ORDER_UNSORT) {
                setSortOrder(SORT_ORDER_ASC);

            } else if (sortOrder === SORT_ORDER_ASC) {
                setSortOrder(SORT_ORDER_DESC);

            } else {
                setSortOrder(SORT_ORDER_UNSORT);

            }
        } else if (colDef !== activeColDef) {
            setSortOrder(SORT_ORDER_ASC);
        }
        setActiveColDef(colDef);
    };

    return (
        <div className="polly-table-container">
            <div className="polly-grid-input">
                <input
                    disabled={disabledSearch}
                    placeholder="Search"
                    type="text"
                    onChange={(event) => handleSearch(event.target.value)}
                    value={filterTextInput}
                />
            </div>
            <div className="polly-grid-table" style={{gridTemplateColumns: `repeat(${columnDefinitions.length}, minmax(100px , 1fr))`}}>
                <div className="polly-grid-header">
                    <div className="polly-grid-tr">
                        {columnDefinitions.map((colDef) => (
                            <div className="polly-grid-th"
                                 onClick={() => changeSorting(colDef)} key={colDef.field}>
                                <div className="polly-grid-div">
                                    {colDef.headerName}
                                    {Object.is(activeColDef,colDef) && activeColDef.sort !== false &&
                                        <div className="polly-grid-th-sort-icon">
                                            { sortOrder === SORT_ORDER_ASC && '🡩'}
                                            { sortOrder === SORT_ORDER_DESC && '🡫'}
                                        </div>
                                    }
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
                <div className="polly-grid-body">
                    {displayRowData.length > 0 &&
                        displayRowData.filter((row) => {
                            if(filterCriteria.length < 3){
                                return row;
                            }
                            return columnDefinitions.some(colDef => {
                                let rowValue = getValueOfRow(row, colDef);

                                if (typeof rowValue !== 'string' && typeof rowValue !== "number") {
                                    return false
                                }
                                return rowValue.toString().toUpperCase().includes(filterCriteria.toUpperCase())
                            })
                        }).map((row, index) => (
                            <div className="polly-grid-tr"
                                 key={index}>
                                {columnDefinitions.map((colDef) => {
                                    return (
                                        <div className="polly-grid-td"
                                             key={colDef.field}>
                                            {getValueOfRow(row, colDef)}
                                        </div>
                                    )
                                })}
                            </div>
                        ))}
                </div>
            </div>
        </div>
    );


};
export default DataGrid;
