import React from "react";
import { useState } from "react";
import DataGrid from "../data-grid/DataGrid";
import "../../style/module.css"
import { ColumnDefinition } from "../../Types/Types";
const AdminAdministration = () => {
    // ??
    const [number, setNumber] = useState<number>();
    const [adminTableData, setAdminTableData] = useState([]);
    const adminHandleChange = event => {
        setNumber(parseFloat(event.target.value));
    }
    const adminOnSubmitHandler = async (event) => {
        event.preventDefault()
        if(number >= 1 && number % 1 === 0 ) {
            try {
                const response = await fetch(
                    `https://randomuser.me/api/?results=${number}`
                )
                if (response.ok) {
                    const data = await response.json()
                    setAdminTableData(data.results);
                }else {
                    throw new Error("Vuznikna greshka")
                }
            } catch (e) {
                alert(e)
            }
        }else{
            alert("Can't use decimal numbers");
        }
    };

    const adminColDefs: ColumnDefinition[] = [
        {
            headerName: 'Email',
            field: 'email'
        },
        {
            headerName: 'Name',
            field: 'name',
            valueGetter: (row) => { return `${row.name.first}, ${row.name.last}` }
        },
        {
            headerName: 'Address',
            field: 'location',
            valueGetter: row => { return `${row.location.city}, ${row.location.state}, ${row.location.country}`}
        },
        {
            headerName: 'LogIn',
            field: 'login',
            valueGetter: (row) => { return `${row.login.username}, ${row.login.password}` },
            sort: false
        },
        {
            headerName: 'Picture',
            field: 'picture',
            valueGetter: (row) => { return row.picture.thumbnail },
            render: (value, row) => (<img src={value as string} alt={value as string} />),
            sort: false
        }
    ]

    return (
        <div className="polly-grid-container">
            <form onSubmit={adminOnSubmitHandler}>
                <input
                    placeholder="Number of people for admin"
                    type="number"
                    min="1"
                    step="0.1"
                    onChange={adminHandleChange}
                    value={number || ""}
                />
                <button onClick={adminOnSubmitHandler}>Load Admin Table</button>
            </form>
            <DataGrid disabledSearch={false} searchDelay={500} rowData={adminTableData} columnDefinitions={adminColDefs} />
        </div>
    );
}
export default AdminAdministration;
