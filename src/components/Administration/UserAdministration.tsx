import React from "react";
import { useState } from "react";
import DataGrid from "../data-grid/DataGrid";
import "../../style/module.css"
import { ColumnDefinition, RowData } from "../../Types/Types";

const UserAdministration = () => {
    const [num, setNum] = useState<number>();
    const [tableData, setTableData] = useState<Array<string[]>>([]);
    const handleChange = event => {
        setNum(parseFloat(event.target.value));
    }
    const onSubmitHandler = async (e) => {
        e.preventDefault()
        if(num >= 1 && num % 1 === 0 ) {
            try {
                const response = await fetch(
                    `https://randomuser.me/api/?results=${num}`
                )
                if (response.ok) {
                    const data = await response.json()
                    setTableData(data.results);
                }else {
                    throw new Error("Vuznikna greshka")
                }
            } catch (e) {
                alert(e)
            }
        }else{
            alert("Can't use decimal numbers");
        }
    };
       const colDefs: ColumnDefinition[] =[
           {
               headerName : "Gender",
               field: "gender",
           },
           {
               headerName: 'Email',
               field: 'email',
           },
           {
               headerName: 'Name',
               field: 'name',
               valueGetter: (row) => { return `${row.name.first}, ${row.name.last}` },
               // valueGetter?: (row: RowData) => string | number;
           },
           {
               headerName: 'Birthday',
               field: 'dob',
               valueGetter: (row) => { return `${row.dob.date}, ${row.dob.age}` },
           },
           {
               headerName: 'Phone',
               field: 'phone',
               sort: false
           },
           {
               headerName: 'Picture',
               field: 'picture',
               valueGetter: (row) => { return row.picture.thumbnail },
               render: (value, row) => (<img src={value as string} alt={value as string} />),
               sort: false
           }
       ]

    return (
        <div className="polly-grid-container">
            <form onSubmit={onSubmitHandler}>
                <input
                    placeholder="Number of people"
                    type="number"
                    min="1"
                    step="0.1"
                    onChange={handleChange}
                    value={num || ""}
                />
                <button onClick={onSubmitHandler}>Load Table</button>
            </form>
            <DataGrid disabledSearch={false} searchDelay={8000} rowData={tableData} columnDefinitions={colDefs} />
        </div>
    );
}
export default UserAdministration;
